from django import forms
from django_countries.fields import CountryField
from django_countries.widgets import CountrySelectWidget

class ShippingAddressForm(forms.Form):
    street_address = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'id': 'floatingInput',
        'placeholder': 'Introduceti adresa',
    }))

    apartment_address = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'id': 'floatingApartment',
        'placeholder': 'Apartament',
    }))
    city = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Oras',
    }))
    state = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Judet',
    }))
    country = CountryField(blank_label='Alegeti tara').formfield(widget=CountrySelectWidget(attrs={
        'class': 'form-control',
        'id': 'floatingSelect',
        'placeholder': 'Alegeti tara',
        'aria-label': 'Alegeti tara',
    }))

    zip_code = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'id': 'floatingZip',
        'placeholder': 'Cod postal',
    }))
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'id': 'floatingEmail',
        'placeholder': 'Email',
    }))
    phone_number = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'id': 'floatingPhoneNumber',
        'placeholder': 'Numar de telefon',
    }))

class CouponForm(forms.Form):
    code = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Coupon code',
    }))
