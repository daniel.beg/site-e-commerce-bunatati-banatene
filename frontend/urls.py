from django.urls import path
from . import views
from .views import MyOrdersView, about
app_name = 'frontend'

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('product/<slug>/', views.ProductDetailView.as_view(),name='detail'),
    path('add-to-cart/<slug>/',views.add_to_cart,name='add-to-cart'),
    path('remove_single_item/<slug>/',views.remove_single_item,name='remove_single_item'),
    path('summary', views.OrderSummaryView.as_view(), name='summary'),
    path('shipping-address/', views.ShippingAddressView.as_view(), name='shipping-address'),
    path('change-address/', views.change_address, name='change-address'),
    path('add-coupon/', views.addCouponView.as_view(), name='add-coupon'),
    path('remove-coupon/', views.removeCouponView.as_view(), name='remove-coupon'),
    path('payment/', views.PaymentView.as_view(), name='payment'),
    path('my-orders/', MyOrdersView.as_view(), name='my-orders'),
    path('about/', about, name='about'),
]

