from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone

from mainapp import settings
from .forms import ShippingAddressForm, CouponForm

from .models import Item, OrderItem, Order, BillingAddress, Coupon, Payment
from django.views.generic import View, DetailView, ListView
import stripe
import random
import string

# Setez  cheia API pentru Stripe din fișierul de setări
stripe.api_key = settings.STRIPE_SECRET_KEY

def create_order_code():
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=12))

# pagina principala care afiseaza lista de produse
class HomeView(ListView):
    model = Item
    paginate_by = 8
    template_name = 'home.html'

# detalii produse
class ProductDetailView(DetailView):
    model = Item
    template_name = 'detail.html'

@login_required(login_url='/accounts/login/')
def add_to_cart(request, slug):
    item = get_object_or_404(Item, slug=slug)
    order_item, created = OrderItem.objects.get_or_create(
        item=item,
        user=request.user,
        ordered=False,
    )

    order_q = Order.objects.filter(user=request.user, ordered=False)

    if order_q.exists():
        order = order_q[0]

        if order.items.filter(item__slug=item.slug).exists():
            order_item.quantity += 1
            order_item.save()
            messages.info(request, 'Produs adaugat in cos')
            return redirect("frontend:summary")
        else:
            messages.info(request, 'Produs adaugat in cos')
            order.items.add(order_item)
            return redirect("frontend:summary")
    else:
        ordered_date = timezone.now()
        order = Order.objects.create(user=request.user, ordered_date=ordered_date)
        order.items.add(order_item)
        messages.info(request, 'Produs adaugat in cos')
        return redirect("frontend:summary")

class OrderSummaryView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        try:
            current_order = Order.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': current_order
            }
            return render(self.request, 'summary.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, 'Ne pare rau, nu aveti nici o comanda!!!')
            return redirect("/")
        return render(self.request, 'summary.html', context)

@login_required(login_url='/accounts/login/')
def remove_single_item(request, slug):
    item = get_object_or_404(Item, slug=slug)

    order_q = Order.objects.filter(user=request.user, ordered=False)

    if order_q.exists():
        order = order_q[0]

        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(item=item, user=request.user, ordered=False)[0]

            if order_item.quantity > 1:
                order_item.quantity -= 1
                order_item.save()
            else:
                order.items.remove(order_item)

            messages.info(request, 'Produs eliminat din cos')
            return redirect("frontend:summary")

        else:
            messages.info(request, 'Produs eliminat din cos')
            return redirect("frontend:summary", slug=slug)
    else:
        messages.info(request, 'Produs eliminat din cos')
        return redirect("frontend:summary", slug=slug)

class ShippingAddressView(View):
    def get(self, *args, **kwargs):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            form = ShippingAddressForm()
            context = {
                'form': form,
                'order': order,
                'couponform': CouponForm(),
                'display_coupon_form': True
            }
            return render(self.request, 'shipping-address.html', context)
        except ObjectDoesNotExist:
            messages.info(self.request, 'Nu aveti o comanda activa')
            return redirect("frontend:summary")

    def post(self, *args, **kwargs):
        form = ShippingAddressForm(self.request.POST or None)
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            if form.is_valid():
                street_address = form.cleaned_data.get('street_address')
                apartment_address = form.cleaned_data.get('apartment_address')
                city = form.cleaned_data.get('city')
                state = form.cleaned_data.get('state')
                country = form.cleaned_data.get('country')
                zip_code = form.cleaned_data.get('zip_code')
                email = form.cleaned_data.get('email')
                phone_number = form.cleaned_data.get('phone_number')

                billing_address = BillingAddress(
                    user=self.request.user,
                    street_address=street_address,
                    apartment=apartment_address,
                    city=city,
                    state=state,
                    country=country,
                    zip=zip_code,
                    email=email,
                    phone_number=phone_number
                )
                billing_address.save()
                order.billing_address = billing_address
                order.save()
                messages.info(self.request, 'Adresa adaugata cu success')
                return redirect("frontend:payment")
        except ObjectDoesNotExist:
            messages.info(self.request, 'Nu aveti o comanda activa')
            return redirect("frontend:summary")

@login_required(login_url='/accounts/login/')
def change_address(request):
    try:
        order = Order.objects.get(user=request.user, ordered=False)
        order.billing_address = None
        order.save()
        messages.info(request, 'Adresa a fost resetata. Adaugati o noua adresa.')
        return redirect("frontend:shipping-address")
    except ObjectDoesNotExist:
        messages.info(request, 'Nu aveti o comanda activa')
        return redirect("frontend:summary")

def get_coupon(request, code):
    try:
        return Coupon.objects.get(code=code)
    except Coupon.DoesNotExist:
        return None

class addCouponView(View):
    def post(self, *args, **kwargs):
        form = CouponForm(self.request.POST or None)
        if form.is_valid():
            code = form.cleaned_data.get('code')
            try:
                order = Order.objects.get(user=self.request.user, ordered=False)

                if order.total_price() >= 100:
                    coupon = get_coupon(self.request, code)
                    if coupon:
                        order.coupon = coupon
                        order.save()
                        messages.info(self.request, 'Cupon adăugat cu succes')
                    else:
                        messages.warning(self.request, 'Cupon invalid!!!')
                else:
                    messages.warning(self.request, 'Totalul comenzii trebuie să fie de cel puțin 100 Lei pentru a folosi un cupon')

                return redirect("frontend:summary")
            except ObjectDoesNotExist:
                messages.info(self.request, 'Nu aveți o comandă activă')
                return redirect("frontend:summary")
        else:
            messages.warning(self.request, 'Formularul de cupon este invalid')
            return redirect("frontend:summary")

class removeCouponView(View):
    def post(self, *args, **kwargs):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            order.coupon = None
            order.save()
            messages.info(self.request, 'Coupon a fost anulat')
            return redirect("frontend:summary")
        except ObjectDoesNotExist:
            messages.info(self.request, 'Nu aveti o comanda activa')
            return redirect("frontend:summary")

class PaymentView(View):
    def get(self, *args, **kwargs):
        order = Order.objects.get(user=self.request.user, ordered=False)
        if order.billing_address:
            context = {
                'order': order,
                'display_coupon_form': False
            }
            return render(self.request, 'payment.html', context)
        else:
            messages.warning(self.request, 'Please add your billing address')
            return redirect('frontend:shipping-address')

    def post(self, *args, **kwargs):
        order = Order.objects.get(user=self.request.user, ordered=False)
        token = self.request.POST.get('stripeToken')
        amount = int(order.total_price() * 100)

        try:
            charge = stripe.Charge.create(
                amount=amount,
                currency='ron',
                source=token,
                description='Plata Bunatati-Banatene',
            )
            payment = Payment()
            payment.stripe_charge_id = charge['id']
            payment.user = self.request.user
            payment.amount = order.total_price()
            payment.save()

            order_items = order.items.all()
            order_items.update(ordered=True)
            for item in order_items:
                item.save()

            order.ordered = True
            order.payment = payment
            order.order_ref = create_order_code()
            order.save()

            messages.success(self.request, 'Comanda finalizata')
            return redirect("frontend:home")

        except stripe.error.CardError as e:
            body = e.json_body
            err = body.get('error', {})
            messages.warning(self.request, f"{err.get('message')}")
            return redirect("frontend:home")
        except stripe.error.RateLimitError as e:
            messages.warning(self.request, "Rate Limit Error")
            return redirect("frontend:home")
        except stripe.error.InvalidRequestError as e:
            messages.warning(self.request, "Invalid parameters")
            return redirect("frontend:home")
        except stripe.error.AuthenticationError as e:
            messages.warning(self.request, "Not authenticated")
            return redirect("frontend:home")
        except stripe.error.APIConnectionError as e:
            messages.warning(self.request, "Network Error")
            return redirect("frontend:home")
        except stripe.error.StripeError as e:
            messages.warning(self.request, "Something went wrong, you were not charged, please try again!")
            return redirect("frontend:home")
        except Exception as e:
            messages.warning(self.request, "Something went wrong, we will work on it and notify you.")
            return redirect("frontend:home")


class MyOrdersView(LoginRequiredMixin, ListView):
    model = Order
    template_name = 'my_orders.html'
    context_object_name = 'orders'

    def get_queryset(self):
        return Order.objects.filter(user=self.request.user, ordered=True)

def about(request):
    return render(request, 'about.html')